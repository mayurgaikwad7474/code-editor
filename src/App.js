import React, { useState, useEffect } from 'react'
import './App.css'
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/ext-language_tools";
import axios from 'axios'
import Terminal, { ColorMode, LineType } from 'react-terminal-ui';

const App = () => {

  const [value, setvalue] = useState("")
  const [outPut, setoutPut] = useState(null)
  const [terminalLineData, setTerminalLineData] = useState([]);
  const [loading, setloading] = useState(false)

  useEffect(() => {
    const stop = (e) => {
      e.stopPropagation();
      e.preventDefault();
    }

    document.querySelector(".ace_editor").addEventListener("paste", stop, true);
    document.querySelector(".ace_editor").addEventListener("copy", stop, true);
    document.querySelector(".ace_editor").addEventListener("cut", stop, true);
    document.addEventListener('contextmenu', event => event.preventDefault());
  }, [])


  const openFullscreen = () => {
    if (
      document.fullscreenElement ||
      document.webkitFullscreenElement ||
      document.mozFullScreenElement ||
      document.msFullscreenElement
    ) {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    } else {
      let element = document.getElementById('editor');
      if (element.requestFullscreen) {
        element.requestFullscreen()
      } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen()
      } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen()
      } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen()
      }
    }
  }

  const onChange = (e) => {
    setvalue(e)
  }

  const runCode = async () => {
    try {
      setTerminalLineData([])
      setloading(true)
      let data = {
        type: LineType.Output,
        value: "Compiling your code . . ."
      }
      setTerminalLineData([data])
      const res = await axios.post('http://localhost:4000/', {
        value: value
      })
      setoutPut(res.data)
      data = {
        type: LineType.Output,
        value: res.data.output
      }

      setTerminalLineData([data])
      setloading(false)
    } catch (error) {
      setloading(false)
    }
  }


  return (
    <div className='grid' id="editor">
      <div>
        <button onClick={runCode}>Run Code</button>
        <button onClick={openFullscreen}>Start Test</button>
        <div className='out-put-container'>
          {
            loading ? "Loading ..." : outPut === null ? null : <span style={{ display: 'flex', flexDirection: 'column' }}>
              <span>Cpu Time : {outPut.cpuTime}</span>
              <span>Memory : {outPut.memory}bytes</span>
            </span>
          }
        </div>
      </div>
      <div>
        <AceEditor
          value={value}
          mode="javascript"
          theme="monokai"
          className="editor"
          onChange={onChange}
          fontSize={14}
          showPrintMargin={false}
          showGutter={true}
          highlightActiveLine={true}
          style={{ width: '100%', height: '70vh' }}
        />
        <div className="container">
          <Terminal
            colorMode={ColorMode.Dark}
            lineData={terminalLineData}
          />
        </div>
      </div>
    </div >
  )
}

export default App